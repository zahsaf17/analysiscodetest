document.addEventListener("DOMContentLoaded", function(event) {
    getDataSet("data/Oslo_STL.json", function(dataSet) {
        if( dataSet ) {
            renderChart("STL", JSON.parse(dataSet));
            
        }
    });
    getDataSet("data/Stockholm_ABB.json", function(dataSet) {
        if( dataSet ) {
            renderChart("ABB", JSON.parse(dataSet));
        }
    });
});

function renderChart(id, dataSet) {
    let count = 1;
    let temp = [];

    let data = dataSet.reduce((result, item) => {
        price = item.last;
        if (result.length != 0) result
        temp.push(item)
        
        if(count == 20) {
            let sum = temp.reduce((sum, item) => sum +  item.last, 0);
            result.push({
                x: `${temp[0].date} - ${temp[19].date}`,
                y: sum/count,
                lowBand: sum/count - math.std(temp.map(i => (i.last) * 2)),
                highBand: sum/count + math.std(temp.map(i => (i.last) * 2))
            })
            
            count = 0
            temp = []
        }
        count++
        return result;
    }, [])

    let lowBand = data.reduce((acc, item) =>{
        acc.push({
            x: item.x,
            y: item.lowBand
        });
        return acc;
    },[]);

    let highBand = data.reduce((acc, item) =>{
        acc.push({
            x:item.x,
            y:item.highBand
        });
        return acc;
    },[]);

    let context = document.querySelector("#" + id).getContext('2d');
    let stockChart = new Chart(context, {
        type: 'line',
        data: {
            datasets: [{
                label: id + " SMA",
                fill: false,
                data: data,
                pointRadius: 0,
                borderColor: "#1a5ecc"
            },
        {
            data:highBand,
            label: "High band",
            borderColor: "#FF7F50",
            fill: false,
            pointRadius: 0
        },
        {
            data:lowBand,
            label: "Low band",
            borderColor: "#FFD700",
            fill: false,
            pointRadius: 0
        }
        ]
        },
        options: {
            responsive: false,
            scales: {
                xAxes: [{
                    type: "time",
                    display: true
                }]
            }
        }
    });
}

function getDataSet(url, callback) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = function() {
        if( xhr.status == 200 ) {
            callback(xhr.responseText);
        } else {
            callback(null);
        }
    };
    xhr.send();
}
